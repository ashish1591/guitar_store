function initDatatables(sort_column, order, columnWidth) {
    $(function () {
        columnWidth = columnWidth || [{

            "defaultContent": "-",
            "targets": "_all"
        }]
        console.log(columnWidth)
        table = $('#sample-table-2').DataTable({
            "pagingType": "full_numbers",
            iDisplayLength: 100,
            bJqueryUI: true,
            bServerSide: true,
            processing: true,
            "language": {
                "processing": "<div></div><div></div><div></div><div></div><div></div>"
            },
            sAjaxSource: $('#sample-table-2').data('source'),
            "order": [
                [sort_column, order]
            ],
            "columnDefs": columnWidth,
            
        });

    })
}


