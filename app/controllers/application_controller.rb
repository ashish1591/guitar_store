class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  before_action :set_cart

  def set_cart
    user_id = (current_user.present?) ? current_user.id : nil
    @shopping_cart = Cart.where('id = ?', session[:cart_id]).last
    if @shopping_cart.nil?
      @shopping_cart = Cart.create( )
      session[:cart_id] = @shopping_cart.id
    end
  end

  def redirect_to_root_if__not_admin
    if current_user.present? and !current_user.is_admin
      redirect_to root_path
    end
  end
end
