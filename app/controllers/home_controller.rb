class HomeController < ApplicationController
    before_action :set_product, :only=>[:add_to_cart, :show, :remove_from_cart]
    before_action :set_defaults , only: [:index]
    skip_before_action :verify_authenticity_token , only: [:add_to_cart, :remove_from_cart]
    skip_before_action :authenticate_user!

    def index
        if current_user.present? and current_user.is_admin
            return redirect_to(brands_url)
        end
        @brands= get_brands
        @no_of_strings = (params[:p] == "guitar") ? Guitar.pluck(:number_of_strings).uniq : []
        @products = get_records
    end

    def show
        @related_products = Guitar.all
    end

    def cart
    end

    def add_to_cart
        quantity = params[:qt].present? ? params[:qt].to_i : 1
        unless @product.remaining_quantity.to_i >= quantity
            return render :json=> {:message=> "insufficient inventory"}, :status=> 401
        end

        @shopping_cart.add(@product, @product.price, quantity)
        @product.update(remaining_quantity: (@product.remaining_quantity - quantity))
        render :json=> {:message=> "item added to cart"}, status: :ok
    end

    def remove_from_cart
        quantity = params[:qt].present? ? params[:qt].to_i : 1
        @shopping_cart.remove(@product, quantity)
        @product.update(remaining_quantity: (@product.remaining_quantity + quantity))
        render :json=> {:message=> "item removed from cart"}, status: :ok
    end

    private
        def get_records
            search_obj = {}
            query=""
            if params[:t] == "all"
                query = 'remaining_quantity > 0'
            else
                query = 'remaining_quantity > 0 and guitar_type = :type'
                search_obj[:type] = params[:t]
            end

            if params[:b].present?
                brands = params[:b].split(',')
                query = query + " and brands.name IN (:brands)"
                search_obj[:brands] = brands
            end

            if params[:n].present? and params[:p] == "guitar"
                strings = params[:n].split(',')
                query = query + " and guitars.number_of_strings IN (:stings)"
                search_obj[:stings] = strings
            end

            res = params[:p].classify.constantize.eager_load(:brand).where(query, search_obj).offset(params[:offset].to_i).limit(10)
            res
        end

        def get_brands
            if params[:p].present?
                res= params[:p].classify.constantize.where('remaining_quantity > 0').map{|x| x.brand.name}.uniq 
            end
            res
        end

        def set_product
            @product = params[:pc].classify.constantize.find_by_id(params[:pi])
        end

        def set_defaults
            first_rec = Guitar.where('remaining_quantity > 0').first
            if params[:p].nil?
                params[:p] =$PRODUCTS[0][1]
            end

            if params[:t].nil?
                params[:t] =$GUITAR_TYPES[0][1]
            end

            if params[:b].nil?
                params[:b] = first_rec.brand.name rescue ''
            end

            if params[:n].nil?
                params[:n] = first_rec.number_of_strings.to_s rescue ""
            end

            # binding.pry
        end
end
