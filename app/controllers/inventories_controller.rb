class InventoriesController < ApplicationController
  before_action :set_inventory, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token , only: [:check_quantity_exceeded]
  before_action :redirect_to_root_if__not_admin

  # GET /inventories
  # GET /inventories.json
  def index
    respond_to do |format|
      format.html
      format.json{render json: InventoryDatatable.new(view_context)}
    end
  end

  # GET /inventories/1
  # GET /inventories/1.json
  def show
  end

  # GET /inventories/new
  def new
    @inventory = Inventory.new
  end

  # GET /inventories/1/edit
  def edit
  end

  # POST /inventories
  # POST /inventories.json
  def create
    @inventory = Inventory.new(inventory_params)

    respond_to do |format|
      if @inventory.save
        format.html { redirect_to inventories_url, notice: 'Inventory was successfully created.' }
        format.json { render :show, status: :created, location: @inventory }
      else
        format.html { render :new }
        format.json { render json: @inventory.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /inventories/1
  # PATCH/PUT /inventories/1.json
  def update
    respond_to do |format|
      if @inventory.update(inventory_params)
        format.html { redirect_to inventories_url, notice: 'Inventory was successfully updated.' }
        format.json { render :show, status: :ok, location: @inventory }
      else
        format.html { render :edit }
        format.json { render json: @inventory.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /inventories/1
  # DELETE /inventories/1.json
  def destroy
    @inventory.destroy
    respond_to do |format|
      format.html { redirect_to inventories_url, notice: 'Inventory was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def check_quantity_exceeded
    message= ""
    if params[:is_accessory] == "true"
      product = Accessory.find_by_id(params[:product_id])
    else
      product = Guitar.find_by_id(params[:product_id])
    end
    message = "stock is not available for selected quantity. available stock is #{product.remaining_quantity.to_i}" if(product.remaining_quantity.to_i < params[:quantity].to_i)
    
    render :json => {message: message}, status: :ok
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_inventory
      @inventory = Inventory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def inventory_params
      params.require(:inventory).permit(:is_accessory, :guitar_id, :accessory_id, :stock_status, :quantity, :price_per_unit, :total_price)
    end
end
