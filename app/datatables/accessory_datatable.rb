class AccessoryDatatable
	delegate :params, :link_to, :raw, :number_to_currency, to: :@view
	
	def initialize(view)
		@view = view
	end

	def as_json(options = {})
    {
        sEcho: params[:sEcho].to_i,
        iTotalRecords: Accessory.all.count,
        iTotalDisplayRecords: accessories.total_entries,
        aaData: data
    }
  end


	private

	def data
		accessories.order('accessories.updated_at desc').map do |accessory|
			[

				accessory.serial_number, 
				accessory.name.to_s,
				accessory.description.to_s,
				accessory.brand.name,
				accessory.guitar_type.titleize,
                raw('<span class="hidden-480 "> <img src='"#{accessory.image_url}"' class="width-100 height-100"/></span>'),
				raw("<span class='hidden-480 '>#{accessory.updated_at.strftime("%e %b %Y %I:%M %p")}</span>"),
                link_to(('<div class="hidden-sm hidden-xs action-buttons">
                    <a class="blue hide" href="/accessories/'"#{accessory.id.to_s}"'">
                        <i class="ace-icon fa fa-eye bigger-130"></i>
                    </a>
                    <a class="green" href="/accessories/'"#{accessory.id.to_s}"'/edit">
                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                    </a>
                    <a class="red " href="/accessories/'"#{accessory.id.to_s}"'" data-method ="delete", data-confirm= "Are you sure, you want to delete this accessory?">
                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                    </a>
                    </div>').html_safe)
			]
		end
	end  

	def accessories
		@accessories = fetch_accessories
	end
 	
 	def fetch_accessories
 		accessories = Accessory.eager_load(:brand).order("#{sort_column} #{sort_direction}")
 		accessories = accessories.page(page).per_page(per_page)
 		if params[:sSearch].present?
	      accessories = accessories.where("accessories.serial_number like :search or brands.name like :search or accessories.name like :search or accessories.description like :search or brands.name like :search or guitar_type like :search", search: "%#{params[:sSearch]}%")
        end
        # binding.pry
        return accessories
 	end

 	def page
 		params[:iDisplayStart].to_i / per_page.to_i + 1
 	end

 	def per_page
 		params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
 	end

     def sort_column
 		columns = %w[accessories.serial_number name description brands.name guitar_type ]
 		columns[params[:isortCol_0].to_i]
 	end

 	def sort_direction
 		params[:sSortDir_0] == 'desc' ? ' desc ' : 'asc'
 	end
end