class BrandDatatable
	delegate :params, :link_to, :raw, :number_to_currency, to: :@view
	
	def initialize(view)
		@view = view
	end

	def as_json(options = {})
    {
        sEcho: params[:sEcho].to_i,
        iTotalRecords: Brand.all.count,
        iTotalDisplayRecords: brands.total_entries,
        aaData: data
    }
  end


	private

	def data
		brands.order('updated_at desc').map do |brand|
			[
				brand.name,
				raw("<span class='hidden-480 '>#{brand.updated_at.strftime("%e %b %Y %I:%M %p")}</span>"),
                link_to(('<div class="hidden-sm hidden-xs action-buttons">
                    <a class="blue hide" href="/brands/'"#{brand.id.to_s}"'">
                        <i class="ace-icon fa fa-eye bigger-130"></i>
                    </a>
                    <a class="green" href="/brands/'"#{brand.id.to_s}"'/edit">
                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                    </a>
                    <a class="red " href="/brands/'"#{brand.id.to_s}"'" data-method ="delete", data-confirm= "Are you sure, you want to delete this brand?">
                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                    </a>
                    </div>').html_safe)
			]
		end
	end  

	def brands
		@brands = fetch_brands
	end
 	
 	def fetch_brands
 		brands = Brand.order("#{sort_column} #{sort_direction}")
 		brands = brands.page(page).per_page(per_page)
 		if params[:sSearch].present?
	      brands = brands.where("name like :search", search: "%#{params[:sSearch]}%")
	    end
    return brands
 	end

 	def page
 		params[:iDisplayStart].to_i / per_page.to_i + 1
 	end

 	def per_page
 		params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
 	end

 	def sort_column
 		columns = %w[name updated_at]
 		columns[params[:isortCol_0].to_i]
 	end

 	def sort_direction
 		params[:sSortDir_0] == 'desc' ? ' desc ' : 'asc'
 	end
end