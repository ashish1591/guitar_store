class GuitarDatatable
	delegate :params, :link_to, :raw, :number_to_currency, to: :@view
	
	def initialize(view)
		@view = view
	end

	def as_json(options = {})
    {
        sEcho: params[:sEcho].to_i,
        iTotalRecords: Guitar.all.count,
        iTotalDisplayRecords: guitars.total_entries,
        aaData: data
    }
  end


	private

	def data
		guitars.order('guitars.updated_at desc').map do |guitar|
			[

				guitar.serial_number,
				guitar.name.to_s,
				guitar.description.to_s,
				guitar.brand.name,
				guitar.guitar_type.titleize,
                guitar.number_of_strings,
                raw('<span class="hidden-480 "> <img src='"#{guitar.image_url}"' class="width-100 height-100"/></span>'),
				raw("<span class='hidden-480 '>#{guitar.updated_at.strftime("%e %b %Y %I:%M %p")}</span>"),
                link_to(('<div class="hidden-sm hidden-xs action-buttons">
                    <a class="blue hide" href="/guitars/'"#{guitar.id.to_s}"'">
                        <i class="ace-icon fa fa-eye bigger-130"></i>
                    </a>
                    <a class="green" href="/guitars/'"#{guitar.id.to_s}"'/edit">
                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                    </a>
                    <a class="red " href="/guitars/'"#{guitar.id.to_s}"'" data-method ="delete", data-confirm= "Are you sure, you want to delete this guitar?">
                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                    </a>
                    </div>').html_safe)
			]
		end
	end  

	def guitars
		@guitars = fetch_guitars
	end
 	
 	def fetch_guitars
 		guitars = Guitar.eager_load(:brand).order("#{sort_column} #{sort_direction}")
 		guitars = guitars.page(page).per_page(per_page)
 		if params[:sSearch].present?
	      guitars = guitars.where("guitar_type like :search or number_of_strings like :search or serial_number like :search or brands.name like :search", search: "%#{params[:sSearch]}%")
	    end
    return guitars
 	end

 	def page
 		params[:iDisplayStart].to_i / per_page.to_i + 1
 	end

 	def per_page
 		params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
 	end

     def sort_column
 		columns = %w[serial_number guitar_type number_of_strings guitars.updated_at]
 		columns[params[:isortCol_0].to_i]
 	end

 	def sort_direction
 		params[:sSortDir_0] == 'desc' ? ' desc ' : 'asc'
 	end
end