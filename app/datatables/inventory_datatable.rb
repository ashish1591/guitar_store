class InventoryDatatable
	delegate :params, :link_to, :raw, :number_to_currency, to: :@view
	
	def initialize(view)
		@view = view
	end

	def as_json(options = {})
    {
        sEcho: params[:sEcho].to_i,
        iTotalRecords: Inventory.all.count,
        iTotalDisplayRecords: inventories.total_entries,
        aaData: data
    }
  end


	private

	def data
		inventories.order('inventories.updated_at desc').map do |inventory|
			[
				inventory.product.name,
                inventory.is_accessory ? "Yes" : "No",
                raw('<span class='"#{((inventory.stock_status == "in") ? "green-text" : "red-text")}"'>' "#{inventory.quantity}"'</span>'),
				inventory.price_per_unit.to_s,
				inventory.total_price.to_s,
				raw("<span class='hidden-480 '>#{inventory.updated_at.strftime("%e %b %Y %I:%M %p")}</span>"),
                link_to(('<div class="hidden-sm hidden-xs action-buttons">
                    <a class="blue hide" href="/inventories/'"#{inventory.id.to_s}"'">
                        <i class="ace-icon fa fa-eye bigger-130"></i>
                    </a>
                    <a class="green" href="/inventories/'"#{inventory.id.to_s}"'/edit">
                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                    </a>
                    <a class="red " href="/inventories/'"#{inventory.id.to_s}"'" data-method ="delete", data-confirm= "Are you sure, you want to delete this inventory?">
                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                    </a>
                    </div>').html_safe)
			]
		end
	end  

	def inventories
		@inventories = fetch_inventories
	end
 	
 	def fetch_inventories
 		inventories = Inventory.eager_load([{:guitar => :brand}, {:accessory=> :brand}]).order("#{sort_column} #{sort_direction}")
 		inventories = inventories.page(page).per_page(per_page)
 		if params[:sSearch].present?
	      inventories = inventories.where("accessories.name like :search or guitars.name like :search or cast(quantity as text) ilike :search or cast(price_per_unit as text) ilike :search or cast(total_price as text) ilike :search", search: "%#{params[:sSearch]}%")
	    end
    return inventories
 	end

 	def page
 		params[:iDisplayStart].to_i / per_page.to_i + 1
 	end

 	def per_page
 		params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
 	end

     def sort_column
 		columns = %w[guitars.name is_accessory quantity price_per_unit total_price accessories.name]
 		columns[params[:isortCol_0].to_i]
 	end

 	def sort_direction
 		params[:sSortDir_0] == 'desc' ? ' desc ' : 'asc'
 	end
end