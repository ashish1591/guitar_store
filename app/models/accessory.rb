class Accessory < ApplicationRecord
    belongs_to :brand
    mount_uploader :image, AccessoryUploader
    before_create :set_serial_number

    validates :name, presence: true
    validates :description, presence: true
    validates :brand_id, presence: true
    validates :guitar_type, presence: true
    validates :image, presence: true
    validates :name, uniqueness: { scope: :brand_id }


    def set_serial_number
        pre = "AY"
        number = (rand() * 100000).to_i
        self.serial_number = pre + number.to_s
    end
end
