class Brand < ApplicationRecord
    has_many :guitars
    has_many :accessories

    validates :name, presence: true
    validates :name,  uniqueness: true
end
