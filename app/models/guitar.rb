class Guitar < ApplicationRecord
    belongs_to :brand
    mount_uploader :image, GuitarUploader
    before_create :set_serial_number
    
    validates :name, presence: true
    validates :description, presence: true
    validates :brand_id, presence: true
    validates :guitar_type, presence: true
    validates :number_of_strings, presence: true
    validates :image, presence: true
    validates :name, uniqueness: { scope: :brand_id }

    def set_serial_number
        if self.guitar_type.present?
            pre = (self.guitar_type == "electric") ? "EC" : "AC"
            number = (rand() * 100000).to_i
            self.serial_number = pre + number.to_s
        end
    end
end
