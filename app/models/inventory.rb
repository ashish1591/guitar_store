class Inventory < ApplicationRecord
    belongs_to :guitar, optional: :true
    belongs_to :accessory, optional: :true

    after_save :update_remaining_count_and_price

    def product
        self.is_accessory ? self.accessory : self.guitar
    end

    def update_remaining_count_and_price
       
        product = self.product
        current_quantity = product.remaining_quantity
        updated_quantity = (self.stock_status == "in") ? (current_quantity.to_i + self.quantity.to_i) : (current_quantity.to_i - self.quantity.to_i)
        updated_price = (self.stock_status == "in") ? (self.price_per_unit.to_f) : product.price.to_f
        product.update(remaining_quantity: updated_quantity, price: updated_price)
    end
end
