json.extract! accessory, :id, :brand_id, :name, :guitar_id, :image, :remaining_quantity, :serial_number, :created_at, :updated_at
json.url accessory_url(accessory, format: :json)
