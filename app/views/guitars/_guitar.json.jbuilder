json.extract! guitar, :id, :brand_id, :guitar_type, :number_of_strings, :serial_number, :remaining_quantity, :image, :created_at, :updated_at
json.url guitar_url(guitar, format: :json)
