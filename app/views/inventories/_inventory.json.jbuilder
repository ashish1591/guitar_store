json.extract! inventory, :id, :is_accessory, :guitar_id, :accessory_id, :stock_status, :quantity, :price_per_unit, :total_price, :created_at, :updated_at
json.url inventory_url(inventory, format: :json)
