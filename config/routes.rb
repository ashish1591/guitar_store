Rails.application.routes.draw do
  resources :inventories do
    collection do
      post 'check_quantity_exceeded' => 'inventories#check_quantity_exceeded'
    end
  end
  root "home#index"
  resources :accessories
  resources :guitars
  resources :brands
  # mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users
  post '/add_to_cart'=> 'home#add_to_cart'
  post '/remove_from_cart'=> 'home#remove_from_cart'
  get '/show'=> 'home#show'
  get '/cart'=> 'home#cart'
  # get '/home'=> 'home#index'
  

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
