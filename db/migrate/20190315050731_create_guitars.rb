class CreateGuitars < ActiveRecord::Migration[5.1]
  def change
    create_table :guitars do |t|
      t.integer :brand_id
      t.string :guitar_type
      t.integer :number_of_strings
      t.string :serial_number
      t.integer :remaining_quantity
      t.string :image
      t.string :name
      t.string :description

      t.timestamps
    end
    add_index :guitars, :brand_id
  end
end
