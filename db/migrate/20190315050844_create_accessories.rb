class CreateAccessories < ActiveRecord::Migration[5.1]
  def change
    create_table :accessories do |t|
      t.integer :brand_id
      t.string :name
      t.text :description
      t.string :image
      t.integer :remaining_quantity
      t.string :serial_number
      t.string :guitar_type

      t.timestamps
    end
    add_index :accessories, :brand_id
  end
end
