class CreateInventories < ActiveRecord::Migration[5.1]
  def change
    create_table :inventories do |t|
      t.boolean :is_accessory, :default=> false
      t.integer :guitar_id
      t.integer :accessory_id
      t.string :stock_status, :default=> "in"
      t.integer :quantity
      t.float :price_per_unit
      t.float :total_price

      t.timestamps
    end
    add_index :inventories, :guitar_id
    add_index :inventories, :accessory_id
  end
end
