class AddPriceToGuitar < ActiveRecord::Migration[5.1]
  def change
    add_column :guitars, :price, :float
  end
end
