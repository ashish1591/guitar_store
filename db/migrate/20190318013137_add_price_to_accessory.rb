class AddPriceToAccessory < ActiveRecord::Migration[5.1]
  def change
    add_column :accessories, :price, :float
  end
end
