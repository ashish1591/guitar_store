brands
name
has_many :guitars
has_many :accessories

guitars
brand_id:integer:index
guitar_type (electric/acoustic)
number_of_strings:integer
serial_number
image
remaining_quantity
belongs_to :brand


accessories
brand_id:integer:index
name
guitar_id:integer:index
image
remaining_quantity
serial_number

belongs_to :brand
belongs_to :guitar



inventories
stock_status: in/out
is_accessory:boolean
guitar_id:integer:index
accessory_id:integer:index
quantity:integer
price_per_unit:float
total_price:float

rails g scaffold inventories is_accessory:boolean guitar_id:integre:index accessory_id:integer:index stock_status quantity:integer price_per_unit:float total_price:float

belongs_to :guitar
belongs_to :accessory

after_save :update_remaining_count

def update_remaining_count
    if self.is_accessory
        product = self.accessory
    else
        product = self.guitar
    end
    current_quantity = product.remaining_quantity
    updated_quantity = (self.status == "in") ? (current_quantity + self.quantity) : (current_quantity - self.quantity)
    product.update(remaining_quantity: updated_quantity)
end








