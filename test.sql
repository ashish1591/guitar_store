--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.15
-- Dumped by pg_dump version 9.5.15

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: accessories; Type: TABLE; Schema: public; Owner: ashish
--

CREATE TABLE public.accessories (
    id bigint NOT NULL,
    brand_id integer,
    name character varying,
    description text,
    image character varying,
    remaining_quantity integer,
    serial_number character varying,
    guitar_type character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    price double precision
);


ALTER TABLE public.accessories OWNER TO ashish;

--
-- Name: accessories_id_seq; Type: SEQUENCE; Schema: public; Owner: ashish
--

CREATE SEQUENCE public.accessories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.accessories_id_seq OWNER TO ashish;

--
-- Name: accessories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ashish
--

ALTER SEQUENCE public.accessories_id_seq OWNED BY public.accessories.id;


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: ashish
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.ar_internal_metadata OWNER TO ashish;

--
-- Name: brands; Type: TABLE; Schema: public; Owner: ashish
--

CREATE TABLE public.brands (
    id bigint NOT NULL,
    name character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.brands OWNER TO ashish;

--
-- Name: brands_id_seq; Type: SEQUENCE; Schema: public; Owner: ashish
--

CREATE SEQUENCE public.brands_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.brands_id_seq OWNER TO ashish;

--
-- Name: brands_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ashish
--

ALTER SEQUENCE public.brands_id_seq OWNED BY public.brands.id;


--
-- Name: cart_items; Type: TABLE; Schema: public; Owner: ashish
--

CREATE TABLE public.cart_items (
    id bigint NOT NULL,
    owner_id integer,
    owner_type character varying,
    quantity integer,
    item_id integer,
    item_type character varying,
    price double precision,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.cart_items OWNER TO ashish;

--
-- Name: cart_items_id_seq; Type: SEQUENCE; Schema: public; Owner: ashish
--

CREATE SEQUENCE public.cart_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cart_items_id_seq OWNER TO ashish;

--
-- Name: cart_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ashish
--

ALTER SEQUENCE public.cart_items_id_seq OWNED BY public.cart_items.id;


--
-- Name: carts; Type: TABLE; Schema: public; Owner: ashish
--

CREATE TABLE public.carts (
    id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.carts OWNER TO ashish;

--
-- Name: carts_id_seq; Type: SEQUENCE; Schema: public; Owner: ashish
--

CREATE SEQUENCE public.carts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.carts_id_seq OWNER TO ashish;

--
-- Name: carts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ashish
--

ALTER SEQUENCE public.carts_id_seq OWNED BY public.carts.id;


--
-- Name: guitars; Type: TABLE; Schema: public; Owner: ashish
--

CREATE TABLE public.guitars (
    id bigint NOT NULL,
    brand_id integer,
    guitar_type character varying,
    number_of_strings integer,
    serial_number character varying,
    remaining_quantity integer,
    image character varying,
    name character varying,
    description character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    price double precision
);


ALTER TABLE public.guitars OWNER TO ashish;

--
-- Name: guitars_id_seq; Type: SEQUENCE; Schema: public; Owner: ashish
--

CREATE SEQUENCE public.guitars_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.guitars_id_seq OWNER TO ashish;

--
-- Name: guitars_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ashish
--

ALTER SEQUENCE public.guitars_id_seq OWNED BY public.guitars.id;


--
-- Name: inventories; Type: TABLE; Schema: public; Owner: ashish
--

CREATE TABLE public.inventories (
    id bigint NOT NULL,
    is_accessory boolean DEFAULT false,
    guitar_id integer,
    accessory_id integer,
    stock_status character varying DEFAULT 'in'::character varying,
    quantity integer,
    price_per_unit double precision,
    total_price double precision,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.inventories OWNER TO ashish;

--
-- Name: inventories_id_seq; Type: SEQUENCE; Schema: public; Owner: ashish
--

CREATE SEQUENCE public.inventories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.inventories_id_seq OWNER TO ashish;

--
-- Name: inventories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ashish
--

ALTER SEQUENCE public.inventories_id_seq OWNED BY public.inventories.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: ashish
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO ashish;

--
-- Name: users; Type: TABLE; Schema: public; Owner: ashish
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    is_admin boolean DEFAULT false
);


ALTER TABLE public.users OWNER TO ashish;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: ashish
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO ashish;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ashish
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ashish
--

ALTER TABLE ONLY public.accessories ALTER COLUMN id SET DEFAULT nextval('public.accessories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ashish
--

ALTER TABLE ONLY public.brands ALTER COLUMN id SET DEFAULT nextval('public.brands_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ashish
--

ALTER TABLE ONLY public.cart_items ALTER COLUMN id SET DEFAULT nextval('public.cart_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ashish
--

ALTER TABLE ONLY public.carts ALTER COLUMN id SET DEFAULT nextval('public.carts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ashish
--

ALTER TABLE ONLY public.guitars ALTER COLUMN id SET DEFAULT nextval('public.guitars_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ashish
--

ALTER TABLE ONLY public.inventories ALTER COLUMN id SET DEFAULT nextval('public.inventories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: ashish
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: accessories; Type: TABLE DATA; Schema: public; Owner: ashish
--

COPY public.accessories (id, brand_id, name, description, image, remaining_quantity, serial_number, guitar_type, created_at, updated_at, price) FROM stdin;
4	3	Capo	Maecenas eu ornare augue. Morbi pulvinar sodales tortor eget cursus. Aenean non velit ut dui sagittis pellentesque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Proin convallis ex ante, et volutpat magna dignissim vitae. Vivamus pulvinar, risus sit amet suscipit euismod, sapien quam pellentesque augue, quis molestie ex mauris id metus. Nunc ac ullamcorper lorem. Curabitur sed elementum orci, vitae sodales libero. Praesent imperdiet neque sed tellus facilisis facilisis. Quisque fermentum vestibulum lacus in cursus.\r\n\r\n	a4.jpg	\N	AY88284	acoustic	2019-03-18 05:35:22.709361	2019-03-18 05:35:22.709361	\N
6	2	Guitar Stand	Fusce bibendum accumsan molestie. Nam tortor mi, sollicitudin rutrum elementum at, feugiat vitae dolor. In sed bibendum ipsum. Morbi aliquam nisi ac nibh consequat, in finibus massa condimentum. Vivamus et convallis ligula. Donec a accumsan risus, vitae pulvinar elit. In id fermentum nisl. Donec malesuada risus dolor. Nunc ante ex, porttitor sit amet consectetur a, faucibus sed eros. Mauris et pellentesque libero, sit amet lacinia urna. Aenean efficitur rutrum diam vitae sagittis. Ut id ex pellentesque, pretium dolor vel, maximus nisl. Cras fermentum eu turpis sed eleifend.\r\n\r\n	a6.jpeg	\N	AY71604	all	2019-03-18 05:38:34.191783	2019-03-18 05:38:34.191783	\N
7	4	Guitar Strap.	Praesent vel blandit dui, at cursus eros. Fusce nisl enim, finibus quis ex a, sagittis mattis sem. Pellentesque consectetur massa eget lectus mollis, sed gravida diam lobortis. Phasellus dignissim quis enim ac pulvinar. Donec placerat pharetra leo, vel auctor quam ultrices et. In euismod suscipit neque vel elementum. Proin in mattis diam, id mollis nisl. Maecenas porta arcu tortor, ut malesuada felis congue ac. Vestibulum neque diam, aliquet nec fringilla eget, aliquam vel tortor. Morbi massa urna, egestas eu metus sed, tempor tempor neque. Aliquam euismod turpis eu lectus porta, ut varius risus malesuada. Aenean leo dui, hendrerit eget euismod eget, venenatis euismod augue.	a9.jpeg	\N	AY96617	all	2019-03-18 05:39:06.792849	2019-03-18 05:39:06.792849	\N
1	1	Guitar Strings	Donec eget dolor vitae massa tincidunt scelerisque eu mollis purus. Ut vitae tristique lorem. Nulla felis augue, venenatis vitae justo ut, bibendum facilisis quam. Maecenas sit amet pulvinar eros. Nunc facilisis ipsum id tellus ultricies, ut vehicula turpis tincidunt. Nunc imperdiet, ligula in vehicula ultrices, sem ante malesuada neque, nec pretium nisi libero a nunc. Morbi in eros arcu. Nulla quis est ac nisl sollicitudin consectetur. Duis et blandit nisi.	a1.jpg	100	AY9158	electric	2019-03-18 05:34:00.565962	2019-03-18 06:15:36.161866	200
8	1	GUITAR CABLES	Praesent vel blandit dui, at cursus eros. Fusce nisl enim, finibus quis ex a, sagittis mattis sem. Pellentesque consectetur massa eget lectus mollis, sed gravida diam lobortis. Phasellus dignissim quis enim ac pulvinar. Donec placerat pharetra leo, vel auctor quam ultrices et. In euismod suscipit neque vel elementum. Proin in mattis diam, id mollis nisl. Maecenas porta arcu tortor, ut malesuada felis congue ac. Vestibulum neque diam, aliquet nec fringilla eget, aliquam vel tortor. Morbi massa urna, egestas eu metus sed, tempor tempor neque. Aliquam euismod turpis eu lectus porta, ut varius risus malesuada. Aenean leo dui, hendrerit eget euismod eget, venenatis euismod augue.\r\n\r\n	a6.jpeg	12	AY28526	electric	2019-03-18 05:40:37.682561	2019-03-18 06:14:20.855387	1300
3	3	Tuners	Nulla purus ipsum, hendrerit at tortor eget, lacinia pharetra est. Cras fermentum convallis rhoncus. Donec lacus purus, feugiat non risus semper, ultricies convallis magna. Pellentesque laoreet vehicula erat eget accumsan. Aenean tempor elit dui, eget dapibus enim ornare non. Nullam mattis risus at justo eleifend, eu convallis nisl volutpat. Aliquam ut consectetur erat. Vivamus vel ipsum ante. Aliquam dictum tellus leo, et laoreet velit tincidunt quis. Etiam commodo accumsan turpis faucibus gravida. Suspendisse potenti.\r\n\r\n	a3.jpeg	240	AY1971	all	2019-03-18 05:34:56.606332	2019-03-18 05:45:21.290146	260
2	2	 Guitar String Cutter/String Winder	Etiam pharetra diam eget nulla faucibus, ut rhoncus ligula viverra. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque iaculis libero purus, eu commodo felis blandit ut. Phasellus cursus, libero at suscipit imperdiet, lorem tellus posuere massa, ut aliquet ligula lacus sed nunc. Nulla accumsan pretium leo, id semper arcu tincidunt a. Suspendisse viverra tellus ut mauris consectetur vulputate. Etiam condimentum vestibulum lectus, a scelerisque dolor sodales non. Etiam orci sapien, pharetra eu nisl ac, aliquet dapibus nisi. Donec rhoncus pulvinar eros, eu egestas orci gravida quis. Sed dapibus nisi scelerisque, lobortis nulla vel, porta dui. Ut ultrices blandit aliquam. Praesent nulla massa, congue molestie condimentum vitae, auctor quis urna. Nunc dictum tincidunt porttitor.	a2.jpg	215	AY25490	acoustic	2019-03-18 05:34:28.566042	2019-03-18 05:47:14.314681	1200
5	4	Guitar Humidifier	Maecenas lorem metus, volutpat non nisl vel, tincidunt dapibus felis. Pellentesque consectetur sed neque ut pharetra. Aenean viverra, quam id mollis feugiat, enim augue ornare leo, nec fermentum nunc sem bibendum dolor. Nullam vel mattis velit, condimentum eleifend elit. Sed sollicitudin libero sit amet erat sodales rhoncus. Curabitur fermentum ipsum eu ipsum consequat tempus. Suspendisse potenti. Phasellus non odio quis enim vulputate hendrerit et ac felis. Vestibulum scelerisque dui et pellentesque interdum. Proin commodo neque nisi, eget molestie urna finibus ac. Duis et molestie felis. Maecenas velit risus, fermentum non suscipit a, vestibulum rutrum ante. Donec in ullamcorper mi. Mauris sagittis orci ante, ac dictum odio accumsan ut. Donec egestas vitae nisi aliquet condimentum. Duis eget ipsum porttitor tortor pellentesque lobortis.	a5.jpg	40	AY26044	electric	2019-03-18 05:37:47.702233	2019-03-18 05:46:56.506035	450
\.


--
-- Name: accessories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ashish
--

SELECT pg_catalog.setval('public.accessories_id_seq', 8, true);


--
-- Data for Name: ar_internal_metadata; Type: TABLE DATA; Schema: public; Owner: ashish
--

COPY public.ar_internal_metadata (key, value, created_at, updated_at) FROM stdin;
environment	development	2019-03-18 05:12:56.657468	2019-03-18 05:12:56.657468
\.


--
-- Data for Name: brands; Type: TABLE DATA; Schema: public; Owner: ashish
--

COPY public.brands (id, name, created_at, updated_at) FROM stdin;
1	Martin	2019-03-18 05:20:25.417493	2019-03-18 05:20:25.417493
2	Taylor	2019-03-18 05:20:43.773493	2019-03-18 05:20:43.773493
3	Gibson	2019-03-18 05:20:55.104532	2019-03-18 05:20:55.104532
4	Guild	2019-03-18 05:21:05.80014	2019-03-18 05:21:05.80014
\.


--
-- Name: brands_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ashish
--

SELECT pg_catalog.setval('public.brands_id_seq', 4, true);


--
-- Data for Name: cart_items; Type: TABLE DATA; Schema: public; Owner: ashish
--

COPY public.cart_items (id, owner_id, owner_type, quantity, item_id, item_type, price, created_at, updated_at) FROM stdin;
6	4	Cart	2	1	Guitar	100	2019-03-18 06:23:07.391602	2019-03-18 06:31:21.046418
7	4	Cart	2	3	Guitar	100	2019-03-18 06:35:11.380056	2019-03-18 06:35:11.380056
\.


--
-- Name: cart_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ashish
--

SELECT pg_catalog.setval('public.cart_items_id_seq', 7, true);


--
-- Data for Name: carts; Type: TABLE DATA; Schema: public; Owner: ashish
--

COPY public.carts (id, created_at, updated_at) FROM stdin;
1	2019-03-18 05:13:39.591281	2019-03-18 05:13:39.591281
2	2019-03-18 05:14:10.053573	2019-03-18 05:14:10.053573
3	2019-03-18 05:14:18.991735	2019-03-18 05:14:18.991735
4	2019-03-18 05:28:47.957389	2019-03-18 05:28:47.957389
\.


--
-- Name: carts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ashish
--

SELECT pg_catalog.setval('public.carts_id_seq', 4, true);


--
-- Data for Name: guitars; Type: TABLE DATA; Schema: public; Owner: ashish
--

COPY public.guitars (id, brand_id, guitar_type, number_of_strings, serial_number, remaining_quantity, image, name, description, created_at, updated_at, price) FROM stdin;
2	2	electric	12	EC39578	\N	g2.jpeg	Kurt Cobain’s Fender Jag-Stang 	Mauris risus nunc, tempor sed aliquam nec, vestibulum cursus mauris. Curabitur ex urna, lacinia sit amet lorem non, rutrum viverra metus. Donec lacus ipsum, elementum non cursus et, aliquet at dolor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc a nibh iaculis, accumsan turpis non, dapibus orci. Mauris sed efficitur leo. Etiam et maximus nibh. Vestibulum mattis sem non aliquet lobortis. Cras aliquet massa ut elit facilisis, sed mollis diam maximus. Proin elementum a felis vitae elementum.	2019-03-18 05:27:17.847581	2019-03-18 05:27:17.847581	\N
4	3	electric	8	EC99889	\N	g4.png	Bo Diddley's Twang Machine Gretsch.	Suspendisse potenti. Aliquam aliquam, ligula sit amet bibendum molestie, diam elit ornare elit, pellentesque tincidunt dolor ipsum sed augue. Fusce eget consequat nisl, in tristique turpis. Duis hendrerit dolor quis arcu iaculis, ac auctor ligula egestas. Vestibulum pharetra tristique eros, eu consectetur tortor vulputate nec. Etiam efficitur in odio a molestie. Duis elit ex, fringilla nec diam et, vulputate maximus quam. Sed laoreet gravida nisi, sit amet convallis neque pellentesque sed.	2019-03-18 05:28:39.667119	2019-03-18 05:28:39.667119	\N
6	4	acoustic	8	AC39370	\N	g6.jpeg	Eddie Van Halen's Frankenstrat.	Etiam laoreet placerat nunc. Mauris id arcu mi. Nulla nec leo elementum, bibendum elit quis, convallis quam. Suspendisse viverra interdum est vel pellentesque. Nulla et nunc vitae est ultricies ultrices. Integer porttitor imperdiet pulvinar. Aenean convallis ullamcorper mauris eu tincidunt. Proin condimentum ultrices risus a laoreet. Integer sit amet nisl nulla. Suspendisse rhoncus a mi vel efficitur. Cras eget tempor sapien, eu rhoncus arcu. Ut pellentesque augue ac nulla facilisis, non consequat felis gravida. Donec nec eros ac massa volutpat semper ac sed sapien. Nam blandit quam quis augue dictum porttitor. Morbi auctor ex lacus, nec laoreet neque pharetra ac.\r\n\r\n	2019-03-18 05:30:55.76746	2019-03-18 05:30:55.76746	\N
8	4	acoustic	12	AC74780	\N	g8.jpg	Prince’s Cloud	In nec commodo justo. Curabitur metus enim, faucibus non mauris id, varius vulputate massa. Donec at euismod tortor, a egestas orci. Quisque quam massa, fermentum auctor vulputate vitae, sagittis tempor nibh. Curabitur egestas dui sed urna pellentesque, vel posuere arcu molestie. Sed sit amet dolor convallis, finibus ex in, vestibulum nisl. Vestibulum laoreet at turpis non hendrerit. Aliquam consectetur elit odio, ornare ullamcorper turpis rutrum eget. Pellentesque auctor condimentum augue id pretium. Nunc tellus orci, accumsan vitae convallis eget, finibus non magna. Phasellus tincidunt fringilla elit, non tempor ex rhoncus vel. Quisque ac sem placerat, molestie ex vitae, molestie ante. In semper aliquet metus quis viverra.\r\n\r\n	2019-03-18 05:32:17.44574	2019-03-18 05:32:17.44574	\N
1	1	electric	1	EC80551	137	g1.jpg	Dave Gilmour's #0001 Fender Stratocaster	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras auctor nunc sit amet nulla scelerisque dapibus. Curabitur gravida pulvinar purus quis dapibus. Nam eget tortor sit amet dui suscipit condimentum sit amet id neque. In dignissim, ante vitae rutrum mattis, lorem metus vehicula augue, sed lacinia nisl elit vitae orci. Nulla lacinia orci eu tincidunt maximus. Donec nec tincidunt massa. Quisque quis mauris mi. Cras nec augue ut magna sollicitudin faucibus id non dolor. Phasellus sed semper orci, a placerat diam. Suspendisse potenti. Aliquam neque augue, feugiat eu finibus quis, dapibus vitae leo. Pellentesque consequat sapien in est lacinia, tempor vulputate leo sollicitudin. Vivamus ac mollis nunc.	2019-03-18 05:26:13.069341	2019-03-18 06:31:21.063288	5600
7	4	electric	12	EC11165	170	g7.jpg	Gibson Les Paul's Gibson Les Paul.	In nec commodo justo. Curabitur metus enim, faucibus non mauris id, varius vulputate massa. Donec at euismod tortor, a egestas orci. Quisque quam massa, fermentum auctor vulputate vitae, sagittis tempor nibh. Curabitur egestas dui sed urna pellentesque, vel posuere arcu molestie. Sed sit amet dolor convallis, finibus ex in, vestibulum nisl. Vestibulum laoreet at turpis non hendrerit. Aliquam consectetur elit odio, ornare ullamcorper turpis rutrum eget. Pellentesque auctor condimentum augue id pretium. Nunc tellus orci, accumsan vitae convallis eget, finibus non magna. Phasellus tincidunt fringilla elit, non tempor ex rhoncus vel. Quisque ac sem placerat, molestie ex vitae, molestie ante. In semper aliquet metus quis viverra.\r\n\r\n	2019-03-18 05:31:25.219739	2019-03-18 05:44:11.503275	7800
5	3	electric	8	EC89366	105	g5.jpg	Prince's Cloud.	Vestibulum blandit rhoncus lorem, nec lacinia erat tempus a. Vivamus ut nisi vel sem consectetur posuere. Integer pharetra augue id quam lobortis bibendum. Ut efficitur, elit blandit maximus efficitur, magna erat vestibulum sem, vitae ullamcorper elit lectus tincidunt lectus. Nam porttitor eu nibh id mollis. Praesent vel dui hendrerit, pretium velit sit amet, gravida arcu. Integer a dolor libero. Proin venenatis a lacus quis tempus. Curabitur elit nisi, pretium et dui euismod, suscipit bibendum mi. Etiam nec sapien arcu. Aliquam elementum placerat ante eget faucibus. Proin bibendum leo a lorem ornare, vitae efficitur felis auctor.	2019-03-18 05:30:17.310506	2019-03-18 05:44:41.130303	8000
3	2	acoustic	12	AC22670	170	g3.jpg	Kurt Cobain's Fender Jag-Stang	Fusce aliquam faucibus rhoncus. Mauris sagittis placerat dictum. Integer porta urna non quam eleifend, congue vestibulum nisi tempor. Vestibulum quis augue in augue lacinia accumsan eget eu ante. Quisque imperdiet porta bibendum. Donec pellentesque dignissim odio vitae sodales. Nullam eleifend in quam quis hendrerit.	2019-03-18 05:28:01.338366	2019-03-18 06:35:11.394812	40
\.


--
-- Name: guitars_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ashish
--

SELECT pg_catalog.setval('public.guitars_id_seq', 8, true);


--
-- Data for Name: inventories; Type: TABLE DATA; Schema: public; Owner: ashish
--

COPY public.inventories (id, is_accessory, guitar_id, accessory_id, stock_status, quantity, price_per_unit, total_price, created_at, updated_at) FROM stdin;
2	f	1	\N	in	100	1000	100000	2019-03-18 05:42:09.399116	2019-03-18 05:42:09.399116
3	t	\N	1	in	100	200	20000	2019-03-18 05:42:29.536134	2019-03-18 05:42:29.536134
4	f	3	\N	in	100	1200	120000	2019-03-18 05:42:42.219007	2019-03-18 05:42:42.219007
5	f	5	\N	in	50	4000	200000	2019-03-18 05:43:00.294719	2019-03-18 05:43:00.294719
6	f	7	\N	in	50	60000	3000000	2019-03-18 05:43:14.708349	2019-03-18 05:43:14.708349
7	f	3	\N	in	60	4500	270000	2019-03-18 05:43:28.419479	2019-03-18 05:43:28.419479
8	f	5	\N	in	40	6700	268000	2019-03-18 05:43:41.335302	2019-03-18 05:43:41.335302
9	f	1	\N	in	40	5600	224000	2019-03-18 05:43:58.346122	2019-03-18 05:43:58.346122
10	f	7	\N	in	120	7800	936000	2019-03-18 05:44:11.488679	2019-03-18 05:44:11.488679
11	f	3	\N	in	12	40	480	2019-03-18 05:44:26.094323	2019-03-18 05:44:26.094323
12	f	5	\N	in	15	8000	120000	2019-03-18 05:44:41.115114	2019-03-18 05:44:41.115114
13	t	\N	2	in	200	450	90000	2019-03-18 05:44:59.292579	2019-03-18 05:44:59.292579
14	t	\N	3	in	240	260	62400	2019-03-18 05:45:21.281234	2019-03-18 05:45:21.281234
15	t	\N	5	in	28	2800	78400	2019-03-18 05:45:36.6484	2019-03-18 05:45:36.6484
16	t	\N	8	in	12	1300	15600	2019-03-18 05:46:41.35611	2019-03-18 05:46:41.35611
17	t	\N	5	in	12	450	5400	2019-03-18 05:46:56.489894	2019-03-18 05:46:56.489894
18	t	\N	2	in	15	1200	18000	2019-03-18 05:47:14.300036	2019-03-18 05:47:14.300036
\.


--
-- Name: inventories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ashish
--

SELECT pg_catalog.setval('public.inventories_id_seq', 18, true);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: ashish
--

COPY public.schema_migrations (version) FROM stdin;
20190318013137
20190314121909
20190317061919
20190316103133
20190315045931
20190315050731
20190316103216
20190315050844
20190315125629
20190318013125
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: ashish
--

COPY public.users (id, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, created_at, updated_at, is_admin) FROM stdin;
1	ashish@gmail.com	$2a$11$YxMuOQz2Xpch3XEi3WxQT.U5EGMQg/Hh7gtuSzUef9.khCr/vpnDm	\N	\N	\N	2019-03-18 05:14:41.414756	2019-03-18 05:15:35.662725	t
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ashish
--

SELECT pg_catalog.setval('public.users_id_seq', 1, true);


--
-- Name: accessories_pkey; Type: CONSTRAINT; Schema: public; Owner: ashish
--

ALTER TABLE ONLY public.accessories
    ADD CONSTRAINT accessories_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: ashish
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: brands_pkey; Type: CONSTRAINT; Schema: public; Owner: ashish
--

ALTER TABLE ONLY public.brands
    ADD CONSTRAINT brands_pkey PRIMARY KEY (id);


--
-- Name: cart_items_pkey; Type: CONSTRAINT; Schema: public; Owner: ashish
--

ALTER TABLE ONLY public.cart_items
    ADD CONSTRAINT cart_items_pkey PRIMARY KEY (id);


--
-- Name: carts_pkey; Type: CONSTRAINT; Schema: public; Owner: ashish
--

ALTER TABLE ONLY public.carts
    ADD CONSTRAINT carts_pkey PRIMARY KEY (id);


--
-- Name: guitars_pkey; Type: CONSTRAINT; Schema: public; Owner: ashish
--

ALTER TABLE ONLY public.guitars
    ADD CONSTRAINT guitars_pkey PRIMARY KEY (id);


--
-- Name: inventories_pkey; Type: CONSTRAINT; Schema: public; Owner: ashish
--

ALTER TABLE ONLY public.inventories
    ADD CONSTRAINT inventories_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: ashish
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: ashish
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_accessories_on_brand_id; Type: INDEX; Schema: public; Owner: ashish
--

CREATE INDEX index_accessories_on_brand_id ON public.accessories USING btree (brand_id);


--
-- Name: index_guitars_on_brand_id; Type: INDEX; Schema: public; Owner: ashish
--

CREATE INDEX index_guitars_on_brand_id ON public.guitars USING btree (brand_id);


--
-- Name: index_inventories_on_accessory_id; Type: INDEX; Schema: public; Owner: ashish
--

CREATE INDEX index_inventories_on_accessory_id ON public.inventories USING btree (accessory_id);


--
-- Name: index_inventories_on_guitar_id; Type: INDEX; Schema: public; Owner: ashish
--

CREATE INDEX index_inventories_on_guitar_id ON public.inventories USING btree (guitar_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: ashish
--

CREATE UNIQUE INDEX index_users_on_email ON public.users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: ashish
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON public.users USING btree (reset_password_token);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

