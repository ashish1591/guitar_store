require 'test_helper'

class InventoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @inventory = inventories(:one)
  end

  test "should get index" do
    get inventories_url
    assert_response :success
  end

  test "should get new" do
    get new_inventory_url
    assert_response :success
  end

  test "should create inventory" do
    assert_difference('Inventory.count') do
      post inventories_url, params: { inventory: { accessory_id: @inventory.accessory_id, guitar_id: @inventory.guitar_id, is_accessory: @inventory.is_accessory, price_per_unit: @inventory.price_per_unit, quantity: @inventory.quantity, stock_status: @inventory.stock_status, total_price: @inventory.total_price } }
    end

    assert_redirected_to inventory_url(Inventory.last)
  end

  test "should show inventory" do
    get inventory_url(@inventory)
    assert_response :success
  end

  test "should get edit" do
    get edit_inventory_url(@inventory)
    assert_response :success
  end

  test "should update inventory" do
    patch inventory_url(@inventory), params: { inventory: { accessory_id: @inventory.accessory_id, guitar_id: @inventory.guitar_id, is_accessory: @inventory.is_accessory, price_per_unit: @inventory.price_per_unit, quantity: @inventory.quantity, stock_status: @inventory.stock_status, total_price: @inventory.total_price } }
    assert_redirected_to inventory_url(@inventory)
  end

  test "should destroy inventory" do
    assert_difference('Inventory.count', -1) do
      delete inventory_url(@inventory)
    end

    assert_redirected_to inventories_url
  end
end
